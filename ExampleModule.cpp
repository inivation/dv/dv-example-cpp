#include <dv-sdk/module.hpp>

class ExampleModule : public dv::ModuleBase {
public:
	static void initInputs(dv::InputDefinitionList &in) {
		in.addEventInput("events");
	}

	static const char *initDescription() {
		return ("This is an example module that counts positive events and logs the number to DV logging.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("printInterval",
			dv::ConfigOption::intOption(
				"Interval in number of events between consecutive printing of the event number.", 10000, 1, INT32_MAX));
	}

	void run() override {
		const auto printInterval = config.getInt("printInterval");

		const auto events = inputs.getEventInput("events").events();

		for (const auto &event : events) {
			if (event.polarity()) {
				mNumberOfEvents += 1;

				if ((mNumberOfEvents % printInterval) == 0) {
					log.info << "Processed " << mNumberOfEvents << " positive events" << dv::logEnd;
				}
			}
		}
	}

private:
	int64_t mNumberOfEvents = 0;
};

registerModuleClass(ExampleModule)
